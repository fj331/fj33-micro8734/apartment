package br.com.caelum.apartment.infra;

import br.com.caelum.apartment.shared.GenericErrorPayload;
import br.com.caelum.apartment.shared.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@RestControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFoundResource(NotFoundException e) {
        log.warn("[NOT_FOUND] Resource not found: {}", e.getMessage());

        return status(NOT_FOUND).body(buildErrorPayloadFrom(e));
    }

    private GenericErrorPayload buildErrorPayloadFrom(Throwable e) {
        return new GenericErrorPayload(e.getMessage());
    }
}
