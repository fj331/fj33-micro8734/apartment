package br.com.caelum.apartment.api.describe;

import br.com.caelum.apartment.api.describe.DescribeController.ApartmentOutput;
import br.com.caelum.apartment.domain.Apartment;
import br.com.caelum.apartment.infra.rest.BuildingClient;
import br.com.caelum.apartment.shared.ExceptionMessageFormatter;
import br.com.caelum.apartment.shared.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

import static br.com.caelum.apartment.infra.rest.BuildingClient.Building;
import static br.com.caelum.apartment.shared.ExceptionMessageFormatter.template;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
class DescribeService {
    private final static ExceptionMessageFormatter<NotFoundException> NOT_FOUND_EXCEPTION_FORMMATER = template("Cannot find {0} with id {1}", NotFoundException.class);

    private final BuildingClient buildings;
    private final DescribreRepository apartments;
    private final ApartmentToApartmentOutput mapper;

    ApartmentOutput decribeBy(Long id) {
        Apartment apartment = apartments.findById(id).orElseThrow(() -> NOT_FOUND_EXCEPTION_FORMMATER.buildException("apartment", id));

        Long buildingId = apartment.getBuildingId();
        Building building = buildings.findById(buildingId);


        return toApartmentOutput(apartment, building);
    }

    private ApartmentOutput toApartmentOutput(Apartment apartment, Building building) {
        ApartmentOutput apartmentOutput = mapper.map(apartment);
        apartmentOutput.setCondominiumName(building.getCondominiumName());

        return apartmentOutput;
    }

    public List<ApartmentOutput> listAll() {
        List<Apartment> allApartments = apartments.findAll();

        List<Long> allBuildingIds = allApartments.stream().map(Apartment::getBuildingId).collect(toList());

        List<Building> allBuildings = buildings.findAllWhereIdsIn(allBuildingIds);

        Flux<Apartment> apartmentFlux = Flux.fromIterable(allApartments);
        Flux<Building> buildingFlux = Flux.fromIterable(allBuildings);

        return apartmentFlux.zipWith(buildingFlux, this::toApartmentOutput).toStream().collect(toList());
    }

}
