create table apartment(
    id bigint not null auto_increment,
    number int not null ,
    identifier varchar(25),
    building_id bigint not null,

    constraint pk_apartment primary key (id),
    constraint unq_number_identifier_building_id unique (number, identifier, building_id)
);